module ThermalTests

using CSV, DataFrames, PrettyTables

function readdata(dirname, filename)
    return joinpath(dirname..., filename) |> CSV.File |> DataFrame
end

function writedata(data, dirname, filename)
    CSV.write(joinpath(dirname..., filename), data)
    return nothing
end

function tomarkdown(data)
    conf = set_pt_conf(tf = tf_markdown, alignment = :c)
    return pretty_table_with_conf(conf, String, data; header = names(data))
end

function writeio(data, dirname, filename)
    open(joinpath(dirname..., filename), "w") do io
        write(io, data)
    end
    return nothing
end

export readdata, writedata, tomarkdown, writeio

end # module
