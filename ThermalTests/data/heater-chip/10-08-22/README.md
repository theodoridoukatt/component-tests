# Description

TODO :tm:

## Table of Contents

<details>
<summary>Click to expand</summary>

[[_TOC_]]

</details>

## Setup

For this test, a **PDMS chip** bonded to a **glass slide** was used.
A **thermocouple** was attached in the top face of the chip.
Two **polymide heaters** were attached below the glass slide, leaving enough space in the middle.
The whole setup was encased inside a **petri dish**.
The setup was put inside a freezer.

![Test setup drawing](assets/setup.png)
