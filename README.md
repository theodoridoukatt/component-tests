<div align="center">
<p>
    <a href="https://gitlab.com/acubesat/documentation/cdr-public/-/blob/master/DDJF/DDJF_THR_ARPT.pdf">DDJF_THR 📚🧪</a> &bull;
    <a href="https://spacedot.gr/">SpaceDot 🌌🪐</a> &bull;
    <a href="https://acubesat.spacedot.gr/">AcubeSAT 🛰️🌎</a>
</p>
</div>

## Description

Here you can find some preliminary test results towards an effort to better track the testing progress.
The results are as markdown tables (for human parsing) and as CSV tables (for computer parsing).
For the heater CSV files, follow this naming convention:

![Heater test CSV naming convention](assets/heater-naming-convention.png)

Make sure to add a `README.md` file in each test entry (i.e. each `dd-mm-yy` directory) with some concise notes about the test setup and, generally, anything you think is worth noting, an anomaly for an example.
You can add other assets for each entry/`README.md` (e.g. photos, drawings) in the same folder (i.e. the respective `dd-mm-yy` directory).

## Table of Contents

<details>
<summary>Click to expand</summary>

[[_TOC_]]

</details>
